
import logging
import os.path
from datetime import datetime


#Creating log filename
current_time=datetime.now()
logfilename = str(current_time)
logfilename = logfilename.replace(" ", "_")
logfilename = logfilename.replace(":", "_")
logfilename = logfilename.replace(".", "_") + '.log'
logfile = os.path.join('log_dir/', logfilename)
logging.basicConfig(filename=logfile, level=logging.DEBUG, 
        format='%(asctime)s %(levelname)s %(name)s %(threadName)s : %(message)s')