#config.py
import os
import sys
from os import environ as env
import multiprocessing

PORT = int(env.get("PORT", 9084))
DEBUG_MODE = int(env.get("DEBUG_MODE", 1))

#Gunicorn config
bind = ":" + str(PORT)
#workers = multiprocessing.cpu_count() * 2 + 1
workers = multiprocessing.cpu_count() + 1
threads = 2 * multiprocessing.cpu_count()
